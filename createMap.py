import mclevel
import box
from skimage import io, color
from tqdm.auto import tqdm


colors_map = {(0, 0): (2, 0),
           (0, 1): (3, 0),
           (0, 2): (4, 0),
           (0, 3): (5, 0),
           (0, 4): (7, 0),
           (0, 5): (14, 0),
           (0, 6): (15, 0),
           (1, 0): (16, 0),
           (1, 1): (17, 0),
           (1, 2): (21, 0),
           (1, 3): (22, 0),
           (1, 4): (24, 0),
           (1, 5): (35, 0),
           (1, 6): (35, 1),
           (2, 0): (35, 2),
           (2, 1): (35, 3),
           (2, 2): (35, 4),
           (2, 3): (35, 5),
           (2, 4): (35, 6),
           (2, 5): (35, 7),
           (2, 6): (35, 8),
           (3, 0): (35, 9),
           (3, 1): (35, 10),
           (3, 2): (35, 11),
           (3, 3): (35, 12),
           (3, 4): (35, 13),
           (3, 5): (35, 14),
           (3, 6): (35, 15),
           (4, 0): (41, 0),
           (4, 1): (42, 0),
           (4, 2): (43, 0),
           (4, 3): (45, 0),
           (4, 4): (46, 0),
           (4, 5): (47, 0),
           (4, 6): (48, 0),
           (5, 0): (49, 0),
           (5, 1): (54, 0),
           (5, 2): (56, 0),
           (5, 3): (57, 0),
           (5, 4): (58, 0),
           (5, 5): (60, 0),
           (5, 6): (61, 0),
           (6, 0): (73, 0),
           (6, 1): (79, 0),
           (6, 2): (80, 0),
           (6, 3): (82, 0),
           (6, 4): (89, 0),
           (6, 5): (103, 0),
           (6, 6): (246, 0)}

#------------------------------------------------------------------------
# import laspy 
# import numpy as np
# import open3d as o3d
# from sklearn.preprocessing import MinMaxScaler
# f = laspy.file.File("/Users/nikitakrutoy/Downloads/1_Small.las")

# colors = f.points["point"][["red", "green", "blue"]]
# points = f.points["point"][["X", "Y", "Z"]]
# points = np.array(points.tolist())
# points = points / 1000
# colors = np.array(colors.tolist())
# colors_fit = MinMaxScaler().fit_transform(colors)
# pcd = o3d.geometry.PointCloud()
# pcd.points = o3d.utility.Vector3dVector(points)
# pcd.colors = o3d.utility.Vector3dVector(colors_fit)
# pcd.estimate_normals()

# voxel_grid = o3d.geometry.VoxelGrid.create_from_point_cloud(pcd,
#                                                             voxel_size=0.1)

# box_size = ((voxel_grid.get_max_bound() - voxel_grid.get_min_bound()) / 0.1).astype(int)
#------------------------------------------------------------------------
import numpy as np
indeces = np.load("/Users/nikitakrutoy/alushta/indeces.npy")
box_size = np.load("/Users/nikitakrutoy/alushta/bb.npy")
colors = np.load("/Users/nikitakrutoy/alushta/colors.npy")

map_rgb = io.imread("colour_map.png")
map_lab = color.rgb2lab(map_rgb)
# box_size = (300, 300, 300)
#---------------
filepath = "/Users/nikitakrutoy/Library/Application Support/minecraft/saves/Alushta3"
# filepath = "world5"
level = mclevel.fromFile(filepath)
level.setPlayerGameType(1, "Player")

# level.setPlayerSpawnPosition((0, 200, 0))
spawn = np.array(level.playerSpawnPosition())
# spawn = np.array(level.())

offset = np.array((10, 0, 10))
print "Spawn", spawn

bb = box.BoundingBox((spawn + offset).tolist(), box_size)


def color2block(c):
    distance = 300
    voxel_color = color.rgb2lab([[c]])[0][0]
    for k, map_column in enumerate(map_lab):
        for l, map_pixel in enumerate(map_column):
            delta = color.deltaE_cie76(voxel_color,map_pixel)
            if delta < distance:
                distance = delta
                blockID, data = colors_map[(k,l)]
    return blockID, data

print "Cleaning"
blockID = 0
data = 0
blockInfo = level.materials.blockWithID(blockID, data)
level.fillBlocks(bb, blockInfo)

print "Cleared"

print "Building"
blockID = 1
data = 0
blockInfo = level.materials.blockWithID(blockID, data)
for pos, c in tqdm(zip(indeces, colors)):
    blockInfo = level.materials.blockWithID(*color2block(c))
    bb_ = box.BoundingBox((spawn + offset + pos).tolist(), (1, 1, 1))
    level.fillBlocks(bb_, blockInfo)
level.saveInPlace()
print("Done!")

